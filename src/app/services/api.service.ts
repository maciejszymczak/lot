import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {ListItem, VenuesList} from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class ApiService {


  constructor(private http: HttpClient) {
  }

  private url = 'https://coinmap.org/api/v1/venues/';

  public getData(paginattionParams: any): Observable<VenuesList> {
    let params = new HttpParams({fromObject: paginattionParams});
    return this.http.get<VenuesList>(this.url, {params: params});

  }

  public putData(body: ListItem, id: number): Observable<ListItem> {
    return of({...body, id});

  }
}
