import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListComponent} from "./components/list/list.component";
import {RouterModule, Routes} from "@angular/router";


const routes: Routes = [
  {path: '', component: ListComponent},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {useHash: true})
  ]
})
export class AppRoutingModule {
}
