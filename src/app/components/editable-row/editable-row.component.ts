import {Component, Input, OnInit} from '@angular/core';
import {ListItem} from '../../interfaces';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ApiService} from '../../services/api.service';

@Component({
  selector: '[app-editable-row]',
  templateUrl: './editable-row.component.html',
  styleUrls: ['./editable-row.component.scss']
})
export class EditableRowComponent implements OnInit {

  @Input() item!: ListItem;
  rowData: ListItem;
  editMode: boolean = false;
  formEditRow: FormGroup;

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.rowData = this.item;
  }

  public edit() {
    this.createFormEditRow();
    this.editMode = true;
  }

  public save() {
    this.apiService.putData(this.formEditRow.value, this.item.id).subscribe((response: ListItem) => {
      alert('zapisano zmiany');
      this.rowData = response;
      this.editMode = false;
    });
  }

  public cancel() {
    this.editMode = false;
  }

  private createFormEditRow(): void {
    this.formEditRow = this.fb.group({
      lat: [this.rowData.lat],
      lon: [this.rowData.lon],
      category: [this.rowData.category],
      name: [this.rowData.name],
      created_on: [this.rowData.created_on],
      geolocation_degrees: [this.rowData.geolocation_degrees],
    });
  }
}
