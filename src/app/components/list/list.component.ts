import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {VenuesList, PaginattionParams} from '../../interfaces';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  public headers: string[] = ['id', 'latitude', 'longitude', 'category', 'name', 'created on', 'geolocation', 'action'];
  public list: VenuesList;
  public listPagination: PaginattionParams = {
    limit: 10,
    offset: 0
  };

  constructor(private apiService: ApiService) {
  }

  ngOnInit(): void {
    this.getList();
  }

  public next(): void {
    this.listPagination.offset += 20;
    this.getList();
  }

  public previous(): void {
    this.listPagination.offset -= 20;
    this.getList();
  }

  private getList(): void {
    this.apiService.getData(this.listPagination).subscribe(response => {
      this.list = response;
    });
  }
}
