export interface ListItem {
  id: number;
  lat: number;
  lon: number;
  category: string;
  name: string;
  created_on: number;
  geolocation_degrees: string;
}

export interface VenuesList {
  venues: ListItem[];
}

export interface PaginattionParams {
  limit: number;
  offset: number;
}
